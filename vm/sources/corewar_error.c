/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_error.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adayrabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 15:29:39 by adayrabe          #+#    #+#             */
/*   Updated: 2019/03/26 15:29:39 by adayrabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar_vm.h"
#include <errno.h>

void	throw_error(char *fmt, char *filename)
{
	ft_dprintf(STDERR_FILENO, fmt, filename);
	ft_putendl_fd("", STDERR_FILENO);
	exit(EXIT_FAILURE);
}
